#include <ESP32Servo.h>
#include "deneyap.h" 

Servo myservo;  

 
int pos = 0;    
#define servoPin  D0
 
void setup() {
  
  ESP32PWM::allocateTimer(0);
  ESP32PWM::allocateTimer(1);
  ESP32PWM::allocateTimer(2);
  ESP32PWM::allocateTimer(3);
  myservo.setPeriodHertz(50);    // standard 50 hz servo
  myservo.attach(servoPin, 500, 2400); // attaches the servo on pin D0 to the servo object
  // using default min/max of 1000us and 2000us
  // different servos may require different min/max settings
  // for an accurate 0 to 180 sweep
}
 
void loop() {
 
  for (pos = 0; pos <= 180; pos += 1) { 
    // 
    myservo.write(pos);    
    delay(15);             
  }
  delay(48000);
  for (pos = 180; pos >= 0; pos -= 1) { 
    myservo.write(pos);    
    delay(15);             
  }
}

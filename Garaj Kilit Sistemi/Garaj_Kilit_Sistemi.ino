#include "Keypad.h"
#include <ESP32Servo.h>
#include "deneyap.h"

 Servo myservo;
 #define servoPin  D12

 int kapat =0;
 int pos = 0;
 #define LED_G  D10
 #define LED_R  D9
 #define LED_Y  D8
 #define LED_B  D7
 
  char key=0;

  
 

const byte ROWS = 4; //four rows
const byte COLS = 3; //three columns
String sifre;
char keys[ROWS][COLS] = {
  {'1','2','3'},
  {'4','5','6'},
  {'7','8','9'},
  {'*','0','#'}
};
byte rowPins[ROWS] = {D6, D5, D4, D3}; //satır pinleri
byte colPins[COLS] = {D2, D1, D0}; //sütun pinleri

Keypad keypad = Keypad( makeKeymap(keys), rowPins, colPins, ROWS, COLS );


void setup() {
  // put your setup code here, to run once:
  Serial.begin (9600);
  
  ESP32PWM::allocateTimer(0);
  ESP32PWM::allocateTimer(1);
  ESP32PWM::allocateTimer(2);
  ESP32PWM::allocateTimer(3);
  myservo.setPeriodHertz(50);    // standard 50 hz servo
  myservo.attach(servoPin, 500, 2400);
  
  pinMode(LED_R, OUTPUT);
  pinMode(LED_G, OUTPUT);
  pinMode(LED_B, OUTPUT);
  pinMode(LED_Y, OUTPUT);
  delay(1000);
  Serial.println("Lütfen 5 haneli şifreyi giriniz");
}

void loop() {
  // put your main code here, to run repeatedly:
  digitalWrite(LED_B, HIGH);
  digitalWrite(LED_Y, HIGH);
  digitalWrite(LED_G, LOW);
  digitalWrite(LED_R, LOW);
  char key = keypad.getKey();
 
  if (key){
    sifre=sifre+key;
    Serial.print("Şİfre : ");
    Serial.println(sifre);
    
  
    if(sifre=="32323"){
    digitalWrite(LED_B, LOW);
    digitalWrite(LED_Y, LOW);  
    digitalWrite(LED_G, HIGH);//ŞİFRE DOĞRU
    digitalWrite(LED_R, LOW); 
    
    Serial.println("Gİriş Başarılı");
    delay(5000);
    for (pos = 0; pos <= 180; pos += 1) { 
    
    myservo.write(pos);    
    delay(15);  
    kapat++;           
    }
    
    }else if(sifre.length()>5)
    {
    digitalWrite(LED_B, HIGH);
    digitalWrite(LED_Y, HIGH);  
    digitalWrite(LED_G, HIGH);
    digitalWrite(LED_R, HIGH);
    delay(1000);
    digitalWrite(LED_B, LOW);
    digitalWrite(LED_Y, LOW);  
    digitalWrite(LED_G, LOW);
    digitalWrite(LED_R, LOW);
    delay(1000);
    digitalWrite(LED_B, HIGH);//TEKRAR ŞİFRE GİRİLMESİ BEKLENİYOR
    digitalWrite(LED_Y, HIGH);
    sifre="";
    Serial.println("Lütfen Tekrar Deneyiniz");
    }else{
    digitalWrite(LED_B, LOW);
    digitalWrite(LED_Y, LOW);  
    digitalWrite(LED_G, LOW);
    digitalWrite(LED_R, HIGH); //ŞİFRE YANLIŞ
    sifre="";
    delay(5000);
    }
      
    }

}
